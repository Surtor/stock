package ratushnyak.stock;

import ratushnyak.stock.interfaces.Order;
import ratushnyak.stock.interfaces.Order.OrderType;
import ratushnyak.stock.interfaces.OrderMatcher;
import ratushnyak.stock.interfaces.Stock;
import ratushnyak.stock.interfaces.TradingGateWay;
import ratushnyak.stock.order.OrderBuilder;
import ratushnyak.stock.simple.order.matcher.SimpleOrderMatcher;

public class StockTest {

	public static void main(String[] args) {
		OrderMatcher matcher = new SimpleOrderMatcher();
		Stock stock = new StockImpl(matcher);
		TradingGateWay gw = stock.getTradingGateWay();
		Order buy = new OrderBuilder().setOrderName("AAA").setOrderPrice(100).setOrderType(OrderType.BUY).setOrderValue(1000).build();
		Order sell = new OrderBuilder().setOrderName("AAA").setOrderPrice(100).setOrderType(OrderType.SELL).setOrderValue(100).build();
		Order sell2 = new OrderBuilder().setOrderName("AAA").setOrderPrice(100).setOrderType(OrderType.SELL).setOrderValue(100).build();
		Order sell3 = new OrderBuilder().setOrderName("AAA").setOrderPrice(100).setOrderType(OrderType.SELL).setOrderValue(100).build();
		Order sell4 = new OrderBuilder().setOrderName("AAA").setOrderPrice(100).setOrderType(OrderType.SELL).setOrderValue(100).build();
		Order sell5 = new OrderBuilder().setOrderName("AAA").setOrderPrice(99).setOrderType(OrderType.SELL).setOrderValue(300).build();
		Order sell6 = new OrderBuilder().setOrderName("AAA").setOrderPrice(99).setOrderType(OrderType.SELL).setOrderValue(600).build();
		
		gw.addOrder(buy);
		gw.addOrder(sell);
		gw.addOrder(sell2);
		gw.addOrder(sell3);
		gw.addOrder(sell4);
		gw.addOrder(sell5);
		gw.addOrder(sell6);
		
		buy = new OrderBuilder().setOrderName("BBB").setOrderPrice(100).setOrderType(OrderType.BUY).setOrderValue(1000).build();
		sell = new OrderBuilder().setOrderName("BBB").setOrderPrice(100).setOrderType(OrderType.SELL).setOrderValue(100).build();
		sell2 = new OrderBuilder().setOrderName("BBB").setOrderPrice(100).setOrderType(OrderType.SELL).setOrderValue(100).build();
		sell3 = new OrderBuilder().setOrderName("BBB").setOrderPrice(100).setOrderType(OrderType.SELL).setOrderValue(100).build();
		sell4 = new OrderBuilder().setOrderName("BBB").setOrderPrice(100).setOrderType(OrderType.SELL).setOrderValue(100).build();
		sell5 = new OrderBuilder().setOrderName("BBB").setOrderPrice(99).setOrderType(OrderType.SELL).setOrderValue(300).build();
		sell6 = new OrderBuilder().setOrderName("BBB").setOrderPrice(99).setOrderType(OrderType.SELL).setOrderValue(500).build();
		
		gw.addOrder(buy);
		gw.addOrder(sell);
		gw.addOrder(sell2);
		gw.addOrder(sell3);
		gw.addOrder(sell4);
		gw.addOrder(sell5);
		gw.addOrder(sell6);
		
		stock.getTradeLedger().addOrderTradeListener((ordersTrade, list) -> System.out.println(ordersTrade));
		
		try {
			Thread.sleep(20000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	

}
