package ratushnyak.stock;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.concurrent.CopyOnWriteArrayList;

import ratushnyak.stock.interfaces.BookToStockListener;
import ratushnyak.stock.interfaces.Order;
import ratushnyak.stock.interfaces.OrderBook;
import ratushnyak.stock.interfaces.OrderMatcher;
import ratushnyak.stock.interfaces.OrdersTrade;
import ratushnyak.stock.interfaces.Stock;
import ratushnyak.stock.interfaces.TradeLedger;
import ratushnyak.stock.interfaces.TradingGateWay;
import ratushnyak.stock.order.book.OrderBookImpl;
import ratushnyak.stock.trade.ledger.TradeLedgerImpl;

public class StockImpl implements Stock {
	
	
	private static final int MATCHING_TIME = 1000;//ms
	
	private final OrderMatcher matcher;
	private final TradingGateWay gateWay = new StockTradingGateWay();
	private final TradeLedger tradeLedger = new TradeLedgerImpl();
	
	private final List<BookToStockListener> listeners = new CopyOnWriteArrayList<>();
	
	private Set<OrderBook> books = new ConcurrentSkipListSet<>();
	
	public StockImpl(OrderMatcher matcher) {
		this.matcher = matcher;
		
		matcher.addListener((OrdersTrade ordersTrade) -> tradeLedger.addOrdersTrade(ordersTrade));
		
		Thread matherThread = new Thread(this::runMatch);
		matherThread.setName("Matcher thread");
		matherThread.setDaemon(true);
		matherThread.start();
	}
	
	/**
	 * run in side thread
	 */
	private void runMatch() {
		while(!Thread.currentThread().isInterrupted()) {
			for(OrderBook book : books) {
				book.match(matcher);
			}
			try {
				Thread.sleep(MATCHING_TIME);
			} catch (InterruptedException e) {
				return;
			}
		}
	}
	
	private class StockTradingGateWay implements TradingGateWay {
		@Override
		public void addOrder(Order order) {
			StockImpl.this.addOrder(order);
		}

		@Override
		public void removeOrder(Order order) {
			StockImpl.this.removeOrder(order);	
		}
	}
	
	@Override
	public TradingGateWay getTradingGateWay() {
		return gateWay;
	}

	@Override
	public List<OrderBook> getBooks() {
		List<OrderBook> result = new ArrayList<>();
		result.addAll(books);
		return Collections.unmodifiableList(result);
	}

	@Override
	public OrderBook getBookByName(String bookName) {
		Optional<OrderBook> searchResult = 
			books.stream().filter(arg0 -> bookName.equals(arg0.getOrderBookName())).findFirst();
		return searchResult.orElse(null);
	}

	private boolean addOrderInner(Order order) {
		OrderBook book = getBookByName(order.getOrderName());
		if(book != null) {
			book.addOrder(order);
			
			for(BookToStockListener listener : listeners) {
				listener.bookAdded(book);
			}
			
			return true;
		}
		return false;
	}
	
	@Override
	public void addOrder(Order order) {
		if(addOrderInner(order)) {
			return;
		}
		//double checking
		synchronized(this) {
			if(addOrderInner(order)) {
				return;
			}
			
			books.add(new OrderBookImpl(order.getOrderName()));
			addOrderInner(order);
		}
	}

	@Override
	public void removeOrder(Order order) {
		OrderBook book = getBookByName(order.getOrderName());
		if(book != null) {
			book.removeOrder(order);
		}
	}

	@Override
	public TradeLedger getTradeLedger() {
		return tradeLedger;
	}

	@Override
	public void addBookToStockListener(BookToStockListener listener) {
		listeners.add(listener);	
	}

	@Override
	public void removeBookToStockListener(BookToStockListener listener) {
		listeners.remove(listener);
	}

}
