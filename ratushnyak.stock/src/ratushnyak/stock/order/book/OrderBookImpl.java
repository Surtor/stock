package ratushnyak.stock.order.book;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.Consumer;

import ratushnyak.stock.interfaces.Order;
import ratushnyak.stock.interfaces.OrderBook;
import ratushnyak.stock.interfaces.OrderMatcher;
import ratushnyak.stock.util.Pair;

public class OrderBookImpl implements OrderBook, Comparable<OrderBook>{
	
	private final ReentrantLock locker = new ReentrantLock();
	private final String bookName;
	private final Set<Order> orderSet = new ConcurrentSkipListSet<>();
	
	private final BlockingQueue<Order> addQueue = new LinkedBlockingQueue<>();
	private final BlockingQueue<Order> removeQueue = new LinkedBlockingQueue<>();
	
	private final List<OrderBookListener> listeners = new CopyOnWriteArrayList<>();
	
	private class NonBlockingOrderListChanger implements Runnable {
		
		private final BlockingQueue<Order> queue;
		private final Consumer<Order> changer;
		
		public NonBlockingOrderListChanger(BlockingQueue<Order> queue, Consumer<Order> changer) {
			this.queue = queue;
			this.changer = changer;
		}
		
		@Override
		public void run() {
			while(!Thread.currentThread().isInterrupted()) {
				Order newOrder;
				try {
					newOrder = queue.take();
				} catch (InterruptedException e) {
					//thread exit
					return;
				}
				locker.lock();
				try {
					changer.accept(newOrder);
				}finally {
					locker.unlock();
				}
			}
		}
	}
	
	public OrderBookImpl(String bookName) {
		this.bookName = bookName;
		
		NonBlockingOrderListChanger addOrderChanger = 
					new NonBlockingOrderListChanger(addQueue, ord ->
					  {	  orderSet.add(ord); 
						  bookChanged(ord, OrderBookListener.EventType.ADD);
				      }) ; 
		Thread addOrderThread = new Thread(addOrderChanger);
		addOrderThread.setDaemon(true);
		addOrderThread.setName("Add order thread.");
		addOrderThread.start();
		
		NonBlockingOrderListChanger removeOrderChanger = 
				new NonBlockingOrderListChanger(removeQueue, ord -> 
				{	orderSet.remove(ord);
				bookChanged(ord, OrderBookListener.EventType.REMOVE);
				});
		Thread removeOrderThread = new Thread(removeOrderChanger);
		removeOrderThread.setDaemon(true);
		removeOrderThread.setName("Remove order thread.");
		removeOrderThread.start();
		
	}
	
	@Override
	public String getOrderBookName() {
		return bookName;
	}

	/**
	 *Non blocking add order
	 */
	@Override
	public void addOrder(Order order) {
		addQueue.add(order);
	}
	
	/**
	 *Non blocking remove order
	 */
	@Override
	public void removeOrder(Order order) {
		removeQueue.add(order);
	}

	/**
	 *copy of order list
	 */
	@Override
	public List<Order> getOrderList() {
		List<Order> result = new ArrayList<>();
		result.addAll(orderSet);
		return result;
	}

	@Override
	public void match(OrderMatcher matcher) {
		locker.lock();
		try {
			List<Pair<Order,OrderBookListener.EventType>> matcherResults = matcher.match(orderSet);
			
			//send result to listeners
			for(Pair<Order,OrderBookListener.EventType> res : matcherResults) {
				this.bookChanged(res.getFirst(), res.getSecond());
			}
			
		}
		finally {
			locker.unlock();
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((bookName == null) ? 0 : bookName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		OrderBookImpl other = (OrderBookImpl) obj;
		if (bookName == null) {
			if (other.bookName != null)
				return false;
		} else if (!bookName.equals(other.bookName))
			return false;
		return true;
	}
	
	private void bookChanged(Order order, OrderBookListener.EventType type) {
		for(OrderBookListener listener : listeners) {
			listener.bookChanged(order, type);
		}
	}
	
	@Override
	public void addOrderBookListener(OrderBookListener listener) {
		listeners.add(listener);		
	}

	@Override
	public void removeOrderBookListener(OrderBookListener listener) {
		listeners.remove(listener);
	}

	@Override
	public int compareTo(OrderBook o) {
		return getOrderBookName().compareTo(o.getOrderBookName());
	}

}
