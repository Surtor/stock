package ratushnyak.stock.order.book;

import ratushnyak.stock.interfaces.Order;

public interface OrderBookListener {
	enum EventType{ADD,REMOVE,UPDATE};
	
	void bookChanged(Order order, EventType type);
}
	
