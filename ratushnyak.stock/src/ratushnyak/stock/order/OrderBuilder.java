package ratushnyak.stock.order;

import java.util.concurrent.atomic.AtomicLong;

import ratushnyak.stock.interfaces.Order;

public class OrderBuilder {
	
	private static final AtomicLong orderId = new AtomicLong(0);
	
	private String orderName;
	private int orderPrice = -1;
	private int orderValue = -1;
	private Order.OrderType orderType;
	
	public Order build() {
		if(orderPrice == -1 || orderValue == -1 ||
		   orderType == null || orderName == null) {
			throw new IllegalStateException("Not enough data to build Order!");
		}
		return new OrderImpl(this); 
	}

	public String getOrderName() {
		return orderName;
	}

	public OrderBuilder setOrderName(String orderName) {
		this.orderName = orderName;
		return this;
	}

	public int getOrderPrice() {
		return orderPrice;
	}

	public OrderBuilder setOrderPrice(int orderPrice) {
		this.orderPrice = orderPrice;
		return this;
	}

	public int getOrderValue() {
		return orderValue;
	}

	public OrderBuilder setOrderValue(int orderValue) {
		this.orderValue = orderValue;
		return this;
	}

	public Order.OrderType getOrderType() {
		return orderType;
	}

	public OrderBuilder setOrderType(Order.OrderType orderType) {
		this.orderType = orderType;
		return this;
	}

	public long getOrderId() {
		return orderId.getAndIncrement();
	}
}
