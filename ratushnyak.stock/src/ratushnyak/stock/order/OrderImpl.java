package ratushnyak.stock.order;

import ratushnyak.stock.interfaces.Order;

public class OrderImpl implements Order, Comparable<Order> {
	
	private final long orderId;
	private final String orderName; 
	private final int orderPrice;
	private final OrderType orderType;
	private final int initialOrderValue;
		
	private volatile int orderValue;
	private volatile OrderStatus orderStatus;
	
	OrderImpl(OrderBuilder builder){
		orderId = builder.getOrderId();
		orderName = builder.getOrderName();
		orderPrice = builder.getOrderPrice();
		orderType = builder.getOrderType();
		initialOrderValue = builder.getOrderValue();
		
		orderValue = builder.getOrderValue();
		orderStatus = OrderStatus.NORMAL;
	}
	
	@Override
	public String getOrderName() {
		return orderName;
	}

	@Override
	public long getOrderId() {
		return orderId;
	}

	@Override
	public int getOrderPrice() {
		return orderPrice;
	}

	@Override
	public int getOrderValue() {
		return orderValue;
	}

	@Override
	public int getOrderInitialValue() {
		return initialOrderValue;
	}

	@Override
	public OrderType getOrderType() {
		return orderType;
	}

	@Override
	public OrderStatus getOrderStatus() {
		return orderStatus;
	}

	@Override
	public void execute(int v) {
		if((orderValue - v) < 0) {
			throw new IllegalStateException("Overflow execute!");
		}
		
		orderValue = orderValue - v;
		
		if(orderValue == 0) {
			orderStatus = OrderStatus.EXECUTED;
		}
	}
	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + initialOrderValue;
		result = prime * result + (int) (orderId ^ (orderId >>> 32));
		result = prime * result + ((orderName == null) ? 0 : orderName.hashCode());
		result = prime * result + orderPrice;
		result = prime * result + ((orderType == null) ? 0 : orderType.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		OrderImpl other = (OrderImpl) obj;
		if (initialOrderValue != other.initialOrderValue)
			return false;
		if (orderId != other.orderId)
			return false;
		if (orderName == null) {
			if (other.orderName != null)
				return false;
		} else if (!orderName.equals(other.orderName))
			return false;
		if (orderPrice != other.orderPrice)
			return false;
		if (orderType != other.orderType)
			return false;
		return true;
	}

	@Override
	public int compareTo(Order o) {
		return Long.compare(this.getOrderId(), o.getOrderId());
	}
	
}
