package ratushnyak.stock.orders.trade;

import ratushnyak.stock.interfaces.OrdersTrade;

public class OrdersTradeImpl implements OrdersTrade {
	
	private final long time;
	private final long sellOrderId;
	private final long buyOrderId;
	private final int orderAmount;
	private final int price;
	
	private final String orderName;
	
	OrdersTradeImpl(OrdersTradeBuilder builder){
		time = builder.getTime();
		sellOrderId = builder.getSellOrderId();
		buyOrderId = builder.getBuyOrderId();
		orderAmount = builder.getOrderAmount();
		orderName = builder.getOrderName();
		price = builder.getPrice();
	}
	
	@Override
	public long getTime() {
		return time;
	}

	@Override
	public long getSellOrderId() {
		return sellOrderId;
	}

	@Override
	public long getBuyOrderId() {
		return buyOrderId;
	}

	@Override
	public int getOrderAmount() {
		return orderAmount;
	}

	@Override
	public String getOrderName() {
		return orderName;
	}
	
	@Override
	public int getPrice() {
		return price;
	}

	@Override
	public String toString() {
		return "OrdersTradeImpl [time=" + time + ", sellOrderId=" + sellOrderId + ", buyOrderId=" + buyOrderId
				+ ", orderAmount=" + orderAmount + ", price=" + price + ", orderName=" + orderName + "]";
	}
	
	

}
