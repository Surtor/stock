package ratushnyak.stock.orders.trade;

import ratushnyak.stock.interfaces.OrdersTrade;

public class OrdersTradeBuilder {
	private long time = System.currentTimeMillis();
	private long sellOrderId = -1;
	private long buyOrderId = -1;
	private int orderAmount = -1;
	private String orderName = null;
	private int price = -1;
	
	public OrdersTrade build() {
		if(sellOrderId == -1 || buyOrderId == -1
			|| orderAmount == -1 || orderName == null 
			|| price == -1) {
			throw new IllegalStateException("Not enough data to build OrderTrade!");
		}
		
		return new OrdersTradeImpl(this);
	}
	
	public long getTime() {
		return time;
	}
	public OrdersTradeBuilder setTime(long time) {
		this.time = time;
		return this;
	}
	public long getSellOrderId() {
		return sellOrderId;
	}
	public OrdersTradeBuilder setSellOrderId(long sellOrderId) {
		this.sellOrderId = sellOrderId;
		return this;
	}
	public long getBuyOrderId() {
		return buyOrderId;
	}
	public OrdersTradeBuilder setBuyOrderId(long buyOrderId) {
		this.buyOrderId = buyOrderId;
		return this;
	}
	public int getOrderAmount() {
		return orderAmount;
	}
	public OrdersTradeBuilder setOrderAmount(int orderAmount) {
		this.orderAmount = orderAmount;
		return this;
	}
	public String getOrderName() {
		return orderName;
	}
	
	public OrdersTradeBuilder setOrderName(String orderName) {
		this.orderName = orderName;
		return this;
	}

	public int getPrice() {
		return price;
	}

	public OrdersTradeBuilder setPrice(int price) {
		this.price = price;
		return this;
	}

}
