package ratushnyak.stock.simple.order.matcher;

import ratushnyak.stock.interfaces.OrdersTrade;

public interface OrderMatcherListener {
	public void matched(OrdersTrade ordersTrade);
}
