package ratushnyak.stock.simple.order.matcher;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.stream.Collectors;

import ratushnyak.stock.interfaces.Order;
import ratushnyak.stock.interfaces.Order.OrderStatus;
import ratushnyak.stock.interfaces.OrderMatcher;
import ratushnyak.stock.interfaces.OrdersTrade;
import ratushnyak.stock.order.book.OrderBookListener;
import ratushnyak.stock.order.book.OrderBookListener.EventType;
import ratushnyak.stock.orders.trade.OrdersTradeBuilder;
import ratushnyak.stock.util.Pair;

public class SimpleOrderMatcher implements OrderMatcher {
	private final List<OrderMatcherListener> listeners = 
									new CopyOnWriteArrayList<>();
	
	private final static Comparator<Order> priceComparator = 
			  (order1,order2) -> Integer.compare(order1.getOrderPrice(), order2.getOrderPrice());
			  
    

	@Override
	public List<Pair<Order, EventType>> match(Set<Order> orders) {
		List<Pair<Order, EventType>> result = new ArrayList<>();
		
		List<Order> BuyOrderList = getBuyOrders(orders);
		List<Order> SellOrderList = getSellOrders(orders);
		
		if(isListsEmpty(BuyOrderList, SellOrderList)) {
			return result;
		}
		
		while(BuyOrderList.get(0).getOrderPrice() >= SellOrderList.get(0).getOrderPrice()) {
			Order buyOrder = BuyOrderList.get(0);
			Order sellOrder = SellOrderList.get(0);
			
			int executedValue = Integer.min(buyOrder.getOrderValue(), sellOrder.getOrderValue());
			
			buyOrder.execute(executedValue);
			sellOrder.execute(executedValue);
			
			addOrderToResult(orders, result, buyOrder);
			addOrderToResult(orders, result, sellOrder);
			
			OrdersTrade ot = createOrdersTrade(sellOrder, buyOrder, executedValue);
			OrdersTraderToListener(ot);
			
			if(buyOrder.getOrderStatus() == OrderStatus.EXECUTED) {
				BuyOrderList.remove(buyOrder);
			}
			
			if(sellOrder.getOrderStatus() == OrderStatus.EXECUTED) {
				SellOrderList.remove(sellOrder);
			}
			
			if(isListsEmpty(BuyOrderList, SellOrderList)) {
				break;
			}
		}
		return result;
	}
	
	private void addOrderToResult(Set<Order> orders, List<Pair<Order, EventType>> result,Order order) {
		if(order.getOrderStatus() == OrderStatus.EXECUTED) {
			orders.remove(order);
			result.add(new Pair<Order, OrderBookListener.EventType>(order, EventType.REMOVE));
		}else {
			result.add(new Pair<Order, OrderBookListener.EventType>(order, EventType.UPDATE));
		}
	}
	
	private OrdersTrade createOrdersTrade(Order sellOrder, Order buyOrder, int value) {
		
		if(!sellOrder.getOrderName().equals(buyOrder.getOrderName())) {
			throw new IllegalStateException("Orders name must be equal!");
		}
		
		OrdersTradeBuilder builder = new OrdersTradeBuilder();
		builder.setSellOrderId(sellOrder.getOrderId()).
				setBuyOrderId(buyOrder.getOrderId()).
				setOrderAmount(value).
				setOrderName(buyOrder.getOrderName()).
				setPrice(sellOrder.getOrderPrice());
		return builder.build();
	}
	
	private void OrdersTraderToListener(OrdersTrade ot) {
		for(OrderMatcherListener listener : listeners) {
			listener.matched(ot);
		}
	}
	
	private boolean isListsEmpty(List<Order> BuyOrder, List<Order> SellOrder) {
		return BuyOrder.isEmpty() || SellOrder.isEmpty();
	}
	
	//get Buy order sorted in reverse price order
	private List<Order> getBuyOrders(Set<Order> orders) {
		List<Order> BuyOrder = orders.stream().
				filter(ord -> ord.getOrderType() == Order.OrderType.BUY).
			    sorted(priceComparator.reversed()).collect(Collectors.toList());
		
		return  BuyOrder;
	}
	
	//get Sell order sorted in price order
	private List<Order> getSellOrders(Set<Order> orders) {
		List<Order> SellOrder = orders.stream().
				filter(ord -> ord.getOrderType() == Order.OrderType.SELL).
				sorted(priceComparator).collect(Collectors.toList());
		
		return SellOrder;
	}
	
	@Override
	public void addListener(OrderMatcherListener listener) {
		listeners.add(listener);
	}

	@Override
	public void removeListener(OrderMatcherListener listener) {
		listeners.remove(listener);
	}

}
