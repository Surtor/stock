package ratushnyak.stock.trade.ledger;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import ratushnyak.stock.interfaces.OrdersTrade;
import ratushnyak.stock.interfaces.TradeLedger;

public class TradeLedgerImpl implements TradeLedger {
	
	private final List<OrdersTrade> ordersTradeList = new CopyOnWriteArrayList<OrdersTrade>();
	private final List<TradeLedgerListener> listeners = new CopyOnWriteArrayList<TradeLedgerListener>();
	
	@Override
	public void addOrdersTrade(OrdersTrade orderTrade) {
		ordersTradeList.add(orderTrade);
		for(TradeLedgerListener ls : listeners) {
			ls.addTradeRecord(orderTrade, getOrdersTrade());
		}
	}

	@Override
	public List<OrdersTrade> getOrdersTrade() {
		return Collections.unmodifiableList(ordersTradeList);
	}

	@Override
	public void addOrderTradeListener(TradeLedgerListener listener) {
		listeners.add(listener);
	}

	@Override
	public void removeOrderTradeListener(TradeLedgerListener listener) {
		listeners.remove(listener);
	}

}
