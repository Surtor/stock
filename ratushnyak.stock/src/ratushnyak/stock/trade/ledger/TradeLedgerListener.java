package ratushnyak.stock.trade.ledger;

import java.util.List;

import ratushnyak.stock.interfaces.OrdersTrade;

public interface TradeLedgerListener {
	void addTradeRecord(OrdersTrade ordersTrade, List<OrdersTrade> orderTradeList);
}
