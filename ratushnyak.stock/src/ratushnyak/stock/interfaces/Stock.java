package ratushnyak.stock.interfaces;

import java.util.List;

public interface Stock {
	
	TradingGateWay getTradingGateWay();
	
	TradeLedger getTradeLedger();
	
	List<OrderBook> getBooks();
	
	/**
	 * @return OrderBook by name or null if not exist 
	 */
	OrderBook getBookByName(String bookName);
	void addOrder(Order order);
	void removeOrder(Order order);
	
	void addBookToStockListener(BookToStockListener listener);
	void removeBookToStockListener(BookToStockListener listener);
}
