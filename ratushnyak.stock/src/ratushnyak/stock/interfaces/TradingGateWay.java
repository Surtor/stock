package ratushnyak.stock.interfaces;

public interface TradingGateWay {
	void addOrder(Order order);
	void removeOrder(Order order);
}
