package ratushnyak.stock.interfaces;

import java.util.List;
import java.util.Set;

import ratushnyak.stock.order.book.OrderBookListener;
import ratushnyak.stock.simple.order.matcher.OrderMatcherListener;
import ratushnyak.stock.util.Pair;

public interface OrderMatcher {
	/**
	 * @param orders set of orders
	 * @return list of executed or part-executed orders
	 */
	public List<Pair<Order,OrderBookListener.EventType>> match(Set<Order> orders);
	public void addListener(OrderMatcherListener listener);
	public void removeListener(OrderMatcherListener listener);
}
