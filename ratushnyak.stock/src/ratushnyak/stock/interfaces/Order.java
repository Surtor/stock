package ratushnyak.stock.interfaces;

public interface Order {
	
	enum OrderType{BUY, SELL};
	
	enum OrderStatus{NORMAL, EXECUTED};
	
	String getOrderName();
	long getOrderId();
	int getOrderPrice();
	int getOrderValue();
	int getOrderInitialValue();
	OrderType getOrderType();
	OrderStatus getOrderStatus();
	
	void execute(int v);
}
