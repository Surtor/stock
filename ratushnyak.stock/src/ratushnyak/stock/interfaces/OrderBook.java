package ratushnyak.stock.interfaces;

import java.util.List;

import ratushnyak.stock.order.book.OrderBookListener;

public interface OrderBook {
	String getOrderBookName();
	void addOrder(Order order);
	void removeOrder(Order order);
	List<Order> getOrderList();
	void match(OrderMatcher matcher);
	void addOrderBookListener(OrderBookListener listener);
	void removeOrderBookListener(OrderBookListener listener);
}
