package ratushnyak.stock.interfaces;

public interface BookToStockListener {
	void bookAdded(OrderBook book);
}
