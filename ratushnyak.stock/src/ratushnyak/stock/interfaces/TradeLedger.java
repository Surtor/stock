package ratushnyak.stock.interfaces;

import java.util.List;

import ratushnyak.stock.trade.ledger.TradeLedgerListener;

public interface TradeLedger {
	public void addOrdersTrade(OrdersTrade orderTrade);
	List<OrdersTrade> getOrdersTrade();
	public void addOrderTradeListener(TradeLedgerListener listener);
	public void removeOrderTradeListener(TradeLedgerListener listener);
}
