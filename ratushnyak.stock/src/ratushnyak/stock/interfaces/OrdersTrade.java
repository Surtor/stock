package ratushnyak.stock.interfaces;

public interface OrdersTrade {
	long getTime();
	long getSellOrderId();
	long getBuyOrderId();
	int getOrderAmount();
	int getPrice();
	String getOrderName();
}
