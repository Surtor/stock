package ratushnyak.stock.ui.handlers;

import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;

import ratushnyak.stock.interfaces.Order;
import ratushnyak.stock.interfaces.Stock;
import ratushnyak.stock.interfaces.TradingGateWay;
import ratushnyak.stock.order.OrderBuilder;
public class AddLastOrderHandler {
	
	public static String lastName = "";
	public static int orderPrice;
	public static Order.OrderType type;
	public static int orderValue;

	@Execute
	public void execute(MPart part, Stock stock){
		TradingGateWay gw = stock.getTradingGateWay();
		Order order = new OrderBuilder().
						setOrderName(lastName).
						setOrderPrice(orderPrice).
						setOrderType(type).
						setOrderValue(orderValue).build();
		gw.addOrder(order);
	}
	
	@CanExecute
	public boolean canExecute() {
		
		if(lastName.equals("")) {
			return false;
		}
		
		return true;
	}
	
	public static boolean isServicePartName(String partName) {
		if(partName.equals("Order Books") ||  partName.equals("Trade Ledger")) {
					return true;
		}
		return false;
	}
}
