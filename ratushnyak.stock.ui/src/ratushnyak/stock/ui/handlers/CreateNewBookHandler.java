package ratushnyak.stock.ui.handlers;

import java.security.SecureRandom;

import javax.inject.Inject;

import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.workbench.modeling.EPartService;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.widgets.Shell;

import ratushnyak.stock.interfaces.Order;
import ratushnyak.stock.interfaces.Stock;
import ratushnyak.stock.interfaces.TradingGateWay;
import ratushnyak.stock.order.OrderBuilder;
import ratushnyak.stock.ui.parts.order.dialog.NewOrderDialog;

public class CreateNewBookHandler {
	
	static final SecureRandom secureRandom = new SecureRandom();
	
	@Inject
	private EPartService partService;
	
	@Execute
	public void execute(Stock stock, Shell shell) {
		NewOrderDialog dialog = new NewOrderDialog(shell);
		MPart part = partService.getActivePart();
		if(part != null && !AddLastOrderHandler.isServicePartName(part.getLabel())) {
			dialog.setOrderName(part.getLabel());
		}
		if(dialog.open() != Window.OK) {
			return;
		}
		
		Order order = new OrderBuilder().setOrderName(dialog.getOrderName()).
										 setOrderPrice(dialog.getPrice()).
										 setOrderType(dialog.getOrderType()).
										 setOrderValue(dialog.getAmount()).build();
		
		TradingGateWay gw = stock.getTradingGateWay();
		gw.addOrder(order);
		
		AddLastOrderHandler.lastName = dialog.getOrderName();
		AddLastOrderHandler.orderPrice = dialog.getPrice();
		AddLastOrderHandler.type = dialog.getOrderType();
		AddLastOrderHandler.orderValue = dialog.getAmount();
	}
	
}