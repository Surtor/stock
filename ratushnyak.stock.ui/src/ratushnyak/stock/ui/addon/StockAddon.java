 
package ratushnyak.stock.ui.addon;

import javax.inject.Inject;

import org.eclipse.e4.core.contexts.IEclipseContext;

import ratushnyak.stock.StockImpl;
import ratushnyak.stock.interfaces.OrderMatcher;
import ratushnyak.stock.interfaces.Stock;
import ratushnyak.stock.simple.order.matcher.SimpleOrderMatcher;

/**
 * Add Stock to context
 * @author Sergey
 *
 */
public class StockAddon {

	@Inject
	public void applicationStarted(IEclipseContext context) {
		OrderMatcher matcher = new SimpleOrderMatcher();
		Stock stock = new StockImpl(matcher);
		
		context.set(Stock.class, stock);
	}

}
