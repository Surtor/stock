package ratushnyak.stock.ui.parts.order.dialog;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import ratushnyak.stock.interfaces.Order;

public class NewOrderDialog extends Dialog {
    private Text txtOrderName;
    private Text txtAmount;
    private Text txtPrice;
    private Combo cmbOrderType; 
    
    private String orderName = "";
    private int amount;
    private int price;
    private Order.OrderType orderType = Order.OrderType.BUY;

    public NewOrderDialog(Shell parentShell) {
        super(parentShell);
    }

    @Override
    protected Control createDialogArea(Composite parent) {
        Composite container = (Composite) super.createDialogArea(parent);
        GridLayout layout = new GridLayout(2, false);
        layout.marginRight = 5;
        layout.marginLeft = 10;
        container.setLayout(layout);

        Label nameLabel = new Label(container, SWT.NONE);
        nameLabel.setText("Order name");

        txtOrderName = new Text(container, SWT.BORDER);
        txtOrderName.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false,
                1, 1));
        txtOrderName.setText(orderName);
        txtOrderName.addModifyListener(this::testFields);

        Label amountLabel = new Label(container, SWT.NONE);
        GridData gd_lblNewLabel = new GridData(SWT.LEFT, SWT.CENTER, false,
                false, 1, 1);
        gd_lblNewLabel.horizontalIndent = 1;
        amountLabel.setLayoutData(gd_lblNewLabel);
        amountLabel.setText("Ammount:");

        txtAmount = new Text(container, SWT.BORDER);
        txtAmount.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true,
                false, 1, 1));
        txtAmount.addModifyListener(this::testFields);
        
        Label priceLabel = new Label(container, SWT.NONE);
        gd_lblNewLabel = new GridData(SWT.LEFT, SWT.CENTER, false,
                false, 1, 1);
        gd_lblNewLabel.horizontalIndent = 1;
        priceLabel.setLayoutData(gd_lblNewLabel);
        priceLabel.setText("Price:");

        txtPrice = new Text(container, SWT.BORDER);
        txtPrice.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true,
                false, 1, 1));
        txtPrice.addModifyListener(this::testFields);
        
        //testFields(null);
        Label typeLabel = new Label(container, SWT.NONE);
        gd_lblNewLabel = new GridData(SWT.LEFT, SWT.CENTER, false,
                false, 1, 1);
        gd_lblNewLabel.horizontalIndent = 1;
        typeLabel.setLayoutData(gd_lblNewLabel);
        typeLabel.setText("Type:");
        
        cmbOrderType = new Combo(container, SWT.DROP_DOWN);
        cmbOrderType.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true,
                false, 1, 1));
        String[] items = new String[] { "Buy", "Sell" };
        cmbOrderType.setItems(items);
        cmbOrderType.select(0);
        cmbOrderType.addModifyListener(this::testFields);
        
        return container;
    }
    
    private void testFields(ModifyEvent e) {
    	if(txtOrderName.getText().trim().isEmpty()) {
    		this.getButton(IDialogConstants.OK_ID).setEnabled(false);
    		return;
    	}
    	
     	try {
    		amount = Integer.parseInt(txtAmount.getText());
    		price = Integer.parseInt(txtPrice.getText());
    	}
    	catch(NumberFormatException ex) {
    		this.getButton(IDialogConstants.OK_ID).setEnabled(false);
    		return;
    	}
     	
     	setOrderType();
     	
    	this.getButton(IDialogConstants.OK_ID).setEnabled(true);
    	
    }
    
    private void setOrderType() {
    	if(cmbOrderType.getText().equals("Buy")) {
    		orderType = Order.OrderType.BUY; 
    	} else {
    		orderType = Order.OrderType.SELL;
    	}
    }
    @Override
    protected void createButtonsForButtonBar(Composite parent) {
        createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL, true);
        createButton(parent, IDialogConstants.CANCEL_ID,
                IDialogConstants.CANCEL_LABEL, false);
        this.getButton(IDialogConstants.OK_ID).setEnabled(false);
    }

    @Override
    protected Point getInitialSize() {
        return new Point(450, 300);
    }

    @Override
    protected void okPressed() {
        orderName = txtOrderName.getText();
        super.okPressed();
    }

    public String getOrderName() {
        return orderName;
    }
    
    public void setOrderName(String name) {
        orderName = name;
    }
    
    public int getAmount() {
        return amount;
    }
    
    public int getPrice() {
    	return price;
    }
    
    public Order.OrderType getOrderType() {
    	return orderType;
    }
}
