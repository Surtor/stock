package ratushnyak.stock.ui.parts.order.book;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerComparator;
import org.eclipse.swt.SWT;

import ratushnyak.stock.interfaces.Order;

public class OrderBookComparator extends ViewerComparator {
    private int propertyIndex;
    private static final int DESCENDING = 1;
    private int direction = DESCENDING;

    public OrderBookComparator() {
        this.propertyIndex = 0;
        direction = DESCENDING;
    }

    public int getDirection() {
        return direction == 1 ? SWT.DOWN : SWT.UP;
    }

    public void setColumn(int column) {
        if (column == this.propertyIndex) {
            // Same column as last sort; toggle the direction
            direction = 1 - direction;
        } else {
            // New column; do an ascending sort
            this.propertyIndex = column;
            direction = DESCENDING;
        }
    }

    @Override
    public int compare(Viewer viewer, Object e1, Object e2) {
        Order p1 = (Order) e1;
        Order p2 = (Order) e2;
        int rc = 0;
        switch (propertyIndex) {
        case 0:
            rc = Long.compare(p1.getOrderId(), p2.getOrderId());
            break;
        case 1:
        	rc = Integer.compare(p1.getOrderPrice(), p2.getOrderPrice());
        	break;
        case 2:
        	rc = Integer.compare(p1.getOrderInitialValue(), p2.getOrderInitialValue());
        	break;
        case 3:
        	rc = Integer.compare(p1.getOrderPrice(), p2.getOrderPrice());
        	break;
        case 4:
        	rc = p1.getOrderType().compareTo(p2.getOrderType());
        	break;
        default:
            rc = 0;
        }
        // If descending order, flip the direction
        if (direction == DESCENDING) {
            rc = -rc;
        }
        return rc;
    }

}
