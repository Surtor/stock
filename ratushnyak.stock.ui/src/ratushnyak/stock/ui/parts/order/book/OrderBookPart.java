package ratushnyak.stock.ui.parts.order.book;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.inject.Inject;

import org.eclipse.e4.ui.di.Focus;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.TableColumn;

import ratushnyak.stock.interfaces.Order;
import ratushnyak.stock.interfaces.OrderBook;
import ratushnyak.stock.interfaces.Stock;
import ratushnyak.stock.order.book.OrderBookListener;

public class OrderBookPart {

	private TableViewer tableViewer;
	private OrderBookComparator comparator = new OrderBookComparator();
	//current book
	private OrderBook book;

	@Inject
	private MPart part;
	
	private final OrderBookListener bookListener = new OrderBookListener() {
		@Override
		public void bookChanged(Order order, EventType type) {
			//book.getOrderList();
			Display.getDefault().asyncExec(()->tableViewer.setInput(book.getOrderList()));
		}
	};
	
	@PostConstruct
	public void createComposite(Composite parent, Stock stock) {

		book = stock.getBookByName(part.getLabel());
		
		parent.setLayout(new GridLayout(1, false));

		tableViewer = new TableViewer(parent);
		tableViewer.setComparator(comparator);
		
		book.addOrderBookListener(bookListener);

		newColumn("Id",0);
		newColumn("Price",1);
		newColumn("Value",2);
		newColumn("Initial value",3);
		newColumn("Order Type", 4);
		
		tableViewer.getTable().setHeaderVisible(true);
		tableViewer.getTable().setLinesVisible(true);

		tableViewer.addDoubleClickListener(this::doubleClick);
		
		tableViewer.setContentProvider(ArrayContentProvider.getInstance());
		tableViewer.setLabelProvider(new BooksLabelProvider());
		tableViewer.setInput(book.getOrderList());
		tableViewer.getTable().setLayoutData(new GridData(GridData.FILL_BOTH));
	}
	
	private void newColumn(String columnName, int index) {
		TableViewerColumn viewerColumn = new TableViewerColumn(tableViewer, SWT.LEFT);
		TableColumn column = viewerColumn.getColumn();
		viewerColumn.getColumn().setText(columnName);
		viewerColumn.getColumn().setWidth(100);
		column.addSelectionListener(getSelectionAdapter(column, index));
	}
	
	private void doubleClick (DoubleClickEvent event) {
		
		IStructuredSelection selection = (IStructuredSelection)event.getSelection();
		Order order = (Order)selection.getFirstElement();

		book.removeOrder(order);
	}
	
	private SelectionAdapter getSelectionAdapter(final TableColumn column,
			final int index) {
		SelectionAdapter selectionAdapter = new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				comparator.setColumn(index);
				int dir = comparator.getDirection();
				tableViewer.getTable().setSortDirection(dir);
				tableViewer.getTable().setSortColumn(column);
				tableViewer.refresh();
			}
		};
		return selectionAdapter;
	}
	
	@Focus
	public void setFocus() {
		tableViewer.getTable().setFocus();
	}

	class BooksLabelProvider implements ITableLabelProvider {

		public String getColumnText(Object element, int columnIndex) {
			Order order = (Order) element;
			
			switch (columnIndex) { 
			case 0: 
				return Long.toString(order.getOrderId()); 
			case 1: 
				return Integer.toString(order.getOrderPrice());
			case 2:
				return Integer.toString(order.getOrderValue());
			case 3:
				return Integer.toString(order.getOrderInitialValue());
			case 4:
				return order.getOrderType().toString();
			}
			
			return "";
			 
		}
		@Override
		public void addListener(ILabelProviderListener listener) {
			// TODO Auto-generated method stub
			
		}
		@Override
		public void dispose() {
			// TODO Auto-generated method stub
			
		}
		@Override
		public boolean isLabelProperty(Object element, String property) {
			// TODO Auto-generated method stub
			return false;
		}
		@Override
		public void removeListener(ILabelProviderListener listener) {
			// TODO Auto-generated method stub
			
		}
		@Override
		public Image getColumnImage(Object element, int columnIndex) {
			// TODO Auto-generated method stub
			return null;
		}
	}
	
	@PreDestroy
	private void destroy() {
		book.removeOrderBookListener(bookListener);
	}
}