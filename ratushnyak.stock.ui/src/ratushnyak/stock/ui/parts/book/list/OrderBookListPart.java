package ratushnyak.stock.ui.parts.book.list;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.eclipse.e4.ui.di.Focus;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.e4.ui.model.application.ui.basic.MBasicFactory;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.model.application.ui.basic.MPartStack;
import org.eclipse.e4.ui.workbench.modeling.EModelService;
import org.eclipse.e4.ui.workbench.modeling.EPartService;
import org.eclipse.e4.ui.workbench.modeling.EPartService.PartState;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.TableColumn;

import ratushnyak.stock.interfaces.OrderBook;
import ratushnyak.stock.interfaces.Stock;

public class OrderBookListPart {

	private TableViewer tableViewer;
	private OrderBookListComparator comparator = new OrderBookListComparator();

	@Inject
	private EPartService partService;
	@Inject
	private MApplication application;
	@Inject
	private EModelService modelService;
	
	
	@PostConstruct
	public void createComposite(Composite parent, Stock stock) {
		
		//just for test
		/*
		 * TradingGateWay gw = stock.getTradingGateWay(); Order buy = new
		 * OrderBuilder().setOrderName("AAA").setOrderPrice(100).setOrderType(OrderType.
		 * BUY).setOrderValue(1000).build(); Order sell = new
		 * OrderBuilder().setOrderName("AAA").setOrderPrice(100).setOrderType(OrderType.
		 * SELL).setOrderValue(100).build(); gw.addOrder(buy); gw.addOrder(sell);
		 */
		
		parent.setLayout(new GridLayout(1, false));

		tableViewer = new TableViewer(parent);
		tableViewer.setComparator(comparator);
		
		TableViewerColumn viewerColumn = new TableViewerColumn(tableViewer, SWT.CENTER);
		TableColumn column = viewerColumn.getColumn();
		viewerColumn.getColumn().setText("Books");
		viewerColumn.getColumn().setWidth(200);
		column.addSelectionListener(getSelectionAdapter(column, 0));
		
		
		/*
		 * viewerColumn = new TableViewerColumn(tableViewer, SWT.LEFT); column =
		 * viewerColumn.getColumn(); viewerColumn.getColumn().setText("Last Name");
		 * viewerColumn.getColumn().setWidth(200);
		 * column.addSelectionListener(getSelectionAdapter(column, 1));
		 */
		
		
		tableViewer.getTable().setHeaderVisible(true);
		tableViewer.getTable().setLinesVisible(true);
		
		tableViewer.addDoubleClickListener(this::doubleClick);
	
		
		tableViewer.setContentProvider(ArrayContentProvider.getInstance());
		tableViewer.setLabelProvider(new BooksLabelProvider());
		tableViewer.setInput(stock.getBooks());
		tableViewer.getTable().setLayoutData(new GridData(GridData.FILL_BOTH));
		
		stock.addBookToStockListener(book -> Display.getCurrent().asyncExec(()-> tableViewer.setInput(stock.getBooks())));
	}
	
	private void doubleClick (DoubleClickEvent event) {
		
		IStructuredSelection selection = (IStructuredSelection)event.getSelection();
		OrderBook ob = (OrderBook)selection.getFirstElement();

		if(activatePartByLabel(ob.getOrderBookName())) {
			return;
		}
		
		MPart part = MBasicFactory.INSTANCE.createPart();
		part.setLabel(ob.getOrderBookName());
		part.setCloseable(true);
		part.setContributionURI("bundleclass://ratushnyak.stock.ui/ratushnyak.stock.ui.parts.order.book.OrderBookPart");
		List<MPartStack> stacks = modelService.findElements(application, "ratushnyak.stock.ui.partstack.sample", MPartStack.class, null);
		stacks.get(0).getChildren().add(part);
		partService.showPart(part, PartState.ACTIVATE);
	}
	
	private boolean activatePartByLabel(String label) {
		for(MPart part : partService.getParts()) {
			if(part.getLabel().equals(label)){
				partService.showPart(part, PartState.ACTIVATE);
				return true;
			}
		}
		return false;
	}
	
	private SelectionAdapter getSelectionAdapter(final TableColumn column,
			final int index) {
		SelectionAdapter selectionAdapter = new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				comparator.setColumn(index);
				int dir = comparator.getDirection();
				tableViewer.getTable().setSortDirection(dir);
				tableViewer.getTable().setSortColumn(column);
				tableViewer.refresh();
			}
		};
		return selectionAdapter;
	}
	
	@Focus
	public void setFocus() {
		tableViewer.getTable().setFocus();
	}

	class BooksLabelProvider implements ITableLabelProvider {

		public String getColumnText(Object element, int columnIndex) {
			OrderBook ae = (OrderBook) element;
			
			return ae.getOrderBookName();
			/*
			 * switch (columnIndex) { case 0: return ae.get(0); case 1: return ae.get(1); }
			 */
		}
		@Override
		public void addListener(ILabelProviderListener listener) {
			// TODO Auto-generated method stub
			
		}
		@Override
		public void dispose() {
			// TODO Auto-generated method stub
			
		}
		@Override
		public boolean isLabelProperty(Object element, String property) {
			// TODO Auto-generated method stub
			return false;
		}
		@Override
		public void removeListener(ILabelProviderListener listener) {
			// TODO Auto-generated method stub
			
		}
		@Override
		public Image getColumnImage(Object element, int columnIndex) {
			// TODO Auto-generated method stub
			return null;
		}
	}
}