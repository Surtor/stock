package ratushnyak.stock.ui.parts.trade.ledger;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerComparator;
import org.eclipse.swt.SWT;

import ratushnyak.stock.interfaces.OrdersTrade;

public class TradeLedgerComparator extends ViewerComparator {
    private int propertyIndex;
    private static final int DESCENDING = 1;
    private int direction = DESCENDING;

    public TradeLedgerComparator() {
        this.propertyIndex = 0;
        direction = DESCENDING;
    }

    public int getDirection() {
        return direction == 1 ? SWT.DOWN : SWT.UP;
    }

    public void setColumn(int column) {
        if (column == this.propertyIndex) {
            // Same column as last sort; toggle the direction
            direction = 1 - direction;
        } else {
            // New column; do an ascending sort
            this.propertyIndex = column;
            direction = DESCENDING;
        }
    }

    @Override
    public int compare(Viewer viewer, Object e1, Object e2) {
    	OrdersTrade p1 = (OrdersTrade) e1;
    	OrdersTrade p2 = (OrdersTrade) e2;
        int rc = 0;
        switch (propertyIndex) {
        case 0:
            rc = Long.compare(p1.getTime(), p2.getTime());
            break;
        case 1:
        	rc = Long.compare(p1.getSellOrderId(), p2.getSellOrderId());
        	break;
        case 2:
        	rc = Long.compare(p1.getBuyOrderId(), p2.getBuyOrderId());
        	break;
        case 3:
        	rc = Integer.compare(p1.getOrderAmount(), p2.getOrderAmount());
        	break;
        case 4:
        	rc = Integer.compare(p1.getPrice(), p2.getPrice());
        	break;
        case 5:
        	rc = p1.getOrderName().compareTo(p2.getOrderName());
        	break;
        default:
            rc = 0;
        }
        // If descending order, flip the direction
        if (direction == DESCENDING) {
            rc = -rc;
        }
        return rc;
    }

}
