package ratushnyak.stock.ui.parts.trade.ledger;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.eclipse.e4.ui.di.Focus;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.TableColumn;

import ratushnyak.stock.interfaces.OrdersTrade;
import ratushnyak.stock.interfaces.Stock;
import ratushnyak.stock.interfaces.TradeLedger;
import ratushnyak.stock.trade.ledger.TradeLedgerListener;

public class TradeLedgerPart {

	private TableViewer tableViewer;
	private TradeLedgerComparator comparator = new TradeLedgerComparator();
	//current ledger
	private TradeLedger ledger;
	
	private final TradeLedgerListener ledgerListener = new TradeLedgerListener() {
		@Override
		public void addTradeRecord(OrdersTrade ordersTrade, List<OrdersTrade> orderTradeList) {
			Display.getDefault().asyncExec(()->tableViewer.setInput(orderTradeList));
		}
	};
	
	@PostConstruct
	public void createComposite(Composite parent, Stock stock) {

		ledger = stock.getTradeLedger();
		
		parent.setLayout(new GridLayout(1, false));

		tableViewer = new TableViewer(parent);
		tableViewer.setComparator(comparator);
		
		ledger.addOrderTradeListener(ledgerListener);

		newColumn("Time",0);
		newColumn("Sell order id",1);
		newColumn("Buy order id",2);
		newColumn("Order amount",3);
		newColumn("Price", 4);
		newColumn("Name", 5);
		
		tableViewer.getTable().setHeaderVisible(true);
		tableViewer.getTable().setLinesVisible(true);

		//tableViewer.addDoubleClickListener(this::doubleClick);
		
		tableViewer.setContentProvider(ArrayContentProvider.getInstance());
		tableViewer.setLabelProvider(new BooksLabelProvider());
		tableViewer.setInput(ledger.getOrdersTrade());
		tableViewer.getTable().setLayoutData(new GridData(GridData.FILL_BOTH));
	}
	
	private void newColumn(String columnName, int index) {
		TableViewerColumn viewerColumn = new TableViewerColumn(tableViewer, SWT.LEFT);
		TableColumn column = viewerColumn.getColumn();
		viewerColumn.getColumn().setText(columnName);
		viewerColumn.getColumn().setWidth(100);
		column.addSelectionListener(getSelectionAdapter(column, index));
	}
	
	/*
	 * private void doubleClick (DoubleClickEvent event) {
	 * 
	 * IStructuredSelection selection = (IStructuredSelection)event.getSelection();
	 * Order order = (Order)selection.getFirstElement();
	 * 
	 * book.removeOrder(order); }
	 */
	
	private SelectionAdapter getSelectionAdapter(final TableColumn column,
			final int index) {
		SelectionAdapter selectionAdapter = new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				comparator.setColumn(index);
				int dir = comparator.getDirection();
				tableViewer.getTable().setSortDirection(dir);
				tableViewer.getTable().setSortColumn(column);
				tableViewer.refresh();
			}
		};
		return selectionAdapter;
	}
	
	@Focus
	public void setFocus() {
		tableViewer.getTable().setFocus();
	}

	class BooksLabelProvider implements ITableLabelProvider {

		public String getColumnText(Object element, int columnIndex) {
			OrdersTrade orderTrade = (OrdersTrade) element;
			
			switch (columnIndex) { 
			case 0: 
				Date date = new Date(orderTrade.getTime());
				DateFormat formatter = new SimpleDateFormat("HH:mm:ss.SSS");
				//formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
				String dateFormatted = formatter.format(date);
				return dateFormatted; 
			case 1: 
				return Long.toString(orderTrade.getSellOrderId());
			case 2:
				return Long.toString(orderTrade.getBuyOrderId());
			case 3:
				return Integer.toString(orderTrade.getOrderAmount());
			case 4:
				return Integer.toString(orderTrade.getPrice());
			case 5:
				return orderTrade.getOrderName();
			}
			
			return "";
			 
		}
		@Override
		public void addListener(ILabelProviderListener listener) {
			// TODO Auto-generated method stub
			
		}
		@Override
		public void dispose() {
			// TODO Auto-generated method stub
			
		}
		@Override
		public boolean isLabelProperty(Object element, String property) {
			// TODO Auto-generated method stub
			return false;
		}
		@Override
		public void removeListener(ILabelProviderListener listener) {
			// TODO Auto-generated method stub
			
		}
		@Override
		public Image getColumnImage(Object element, int columnIndex) {
			// TODO Auto-generated method stub
			return null;
		}
	}
	
	@PreDestroy
	private void destroy() {
		ledger.removeOrderTradeListener(ledgerListener);
	}
}